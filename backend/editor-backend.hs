{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Network.Wai as Wai (responseLBS)
import qualified Network.HTTP.Types as HttpTypes (status200)
import qualified Network.Wai.Handler.Warp as Warp (run)

application _ = return $
  Wai.responseLBS HttpTypes.status200 [("Content-Type", "text/plain")] "Hello World"

main = Warp.run 3000 application
