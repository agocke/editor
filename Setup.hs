import Distribution.PackageDescription (BuildInfo)
import Distribution.Simple
import Distribution.Simple.LocalBuildInfo (LocalBuildInfo)
import Distribution.Simple.PreProcess 
import Distribution.Simple.Program.Types
import Distribution.Simple.Utils (findProgramVersion, notice)
import System.Cmd (rawSystem)
import System.FilePath ((</>))

main = defaultMainWithHooks simpleUserHooks { 
  hookedPrograms = [hprotocProgram]
}

hprotocProgram :: Program
hprotocProgram = (simpleProgram "hprotoc") {
  programFindVersion = findProgramVersion "--version" $ \str ->
    -- Invoking "hprotoc --version" gives a string like
    -- "Welcome to protocol-buffers version x ..."
    case words str of
         (_:_:_:_:ver:_) -> ver
         _ -> ""
}
  

ppHprotoc :: BuildInfo -> LocalBuildInfo -> PreProcessor
ppHprotoc _ lbi = PreProcessor {
  platformIndependent = True,
  runPreProcessor = \(inPath, inFile) (outPath, outFile) verbosity -> do
    notice verbosity (inPath </> inFile ++ " compiled to " ++ 
                      outPath </> outFile)
    rawSystem "hprotoc" ["--haskell-out=" ++ outPath, inPath </> inFile]
    return ()
}
